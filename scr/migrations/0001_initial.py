# Generated by Django 2.0.9 on 2018-10-25 14:14

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ingrediente',
            fields=[
                ('idingrediente', models.AutoField(primary_key=True, serialize=False)),
                ('nome', models.CharField(max_length=200)),
            ],
        ),
    ]
