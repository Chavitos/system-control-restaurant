from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from django.http import JsonResponse,HttpResponse


class Home(TemplateView):
    template_name = 'scr/home.html'


class CadIngredientes(TemplateView):
    template_name = 'scr/cadIngredientes.html'


class cadFornec(TemplateView):
    template_name = 'scr/cadFornec.html'


class cadPrato(TemplateView):
    template_name = 'scr/cadPrato.html'


class listFornec(TemplateView):
    template_name = 'scr/listFornec.html'


class regCompraIngred(TemplateView):
    template_name = 'scr/regCompraIngred.html'


class regPedido(TemplateView):
    template_name = 'scr/regPedido.html'


class relatPedido(TemplateView):
    template_name = 'scr/relatPedido.html'

