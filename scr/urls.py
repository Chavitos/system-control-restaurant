from django.urls import path, include
from scr.views import *

urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('cadFornec/', cadFornec.as_view(), name='cadFornec'),
    path('cadIngredientes/', CadIngredientes.as_view(), name='cadIngredientes'),
    path('cadPrato/', cadPrato.as_view(), name='cadPrato'),
    path('listFornec/', listFornec.as_view(), name='listFornec'),
    path('regCompraIngred/', regCompraIngred.as_view(), name='regCompraIngred'),
    path('regPedido/', regPedido.as_view(), name='regPedido'),
    path('relatPedido/', relatPedido.as_view(), name='relatPedido'),
]
