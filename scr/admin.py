from django.contrib import admin
from .models import Ingrediente, Pedido, Prato, Fornecedor, CompraIngrediente, ListaPrato, ListaIngredientes, ListaIngredientesComprados, ListaIngredientesFornecidos


admin.site.register(Ingrediente)
admin.site.register(Pedido)
admin.site.register(Prato)
admin.site.register(Fornecedor)
admin.site.register(CompraIngrediente)
admin.site.register(ListaPrato)
admin.site.register(ListaIngredientes)
admin.site.register(ListaIngredientesComprados)
admin.site.register(ListaIngredientesFornecidos)
