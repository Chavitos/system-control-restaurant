from django import forms
from .models import *

class PedidoForm(forms.ModelForm):

    class Meta:
        model = Pedido
        fields = ('nome_cli', 'telefone_cli', 'endereco_cli', 'opcaopagamento')

class ListaPratoForm(forms.ModelForm):

    class Meta:
        model = ListaPrato
        fields = ('idprato')


class PratoForm(forms.ModelForm):

    class Meta:
        model = Prato
        fields = ('nome', 'preco')

