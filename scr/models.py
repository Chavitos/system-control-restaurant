from django.db import models


class Ingrediente(models.Model):
    idingrediente = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=200)

    def __str__(self):
        return self.nome


class Pedido(models.Model):
    Escolha = (
        (0, 'Dinheiro'),
        (1, 'Cartão'),
    )
    idpedido = models.AutoField(primary_key=True)
    nome_cli = models.CharField(max_length=200)
    telefonecli = models.CharField(max_length=11)
    enderecocli = models.CharField(max_length=300)
    opcaopagamento = models.IntegerField(choices=Escolha)

    def __str__(self):
        return str(self.idpedido)


class Prato(models.Model):
    idprato = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=200)
    preco = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return f'{self.nome} - {self.preco}'


class Fornecedor(models.Model):
    idfornecedor = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=200)

    def __str__(self):
        return self.nome


class CompraIngrediente(models.Model):
    idcompra_ingrediente = models.AutoField(primary_key=True)
    num_nota_fiscal = models.CharField(max_length=200)
    data_compra = models.DateField()
    nome_fornecedor = models.ForeignKey('Fornecedor', models.DO_NOTHING, db_column='nome', blank=True, null=True)

    def __str__(self):
        return self.num_nota_fiscal

class ListaPrato(models.Model):
    idlistaprato = models.AutoField(primary_key=True)
    idpedido = models.ForeignKey('Pedido', models.DO_NOTHING, db_column='idpedido', blank=True, null=True)
    idprato = models.ForeignKey('Prato', models.DO_NOTHING, db_column='idprato', blank=True, null=True)

class ListaIngredientes(models.Model):
    idlistaingrediente = models.AutoField(primary_key=True)
    quantidade = models.IntegerField()
    idprato = models.ForeignKey('Prato', models.DO_NOTHING, db_column='idprato', blank=True, null=True)
    idingrediente = models.ForeignKey('Ingrediente', models.DO_NOTHING, db_column='idingrediente', blank=True, null=True)

class ListaIngredientesFornecidos(models.Model):
    idlistaingredientesfornecidos = models.AutoField(primary_key=True)
    preco = models.DecimalField(max_digits=5, decimal_places=2)
    idingrediente = models.ForeignKey('Ingrediente', models.DO_NOTHING, db_column='idingrediente', blank=True, null=True)
    idfornecedor = models.ForeignKey('Fornecedor', models.DO_NOTHING, db_column='idfornecedor', blank=True, null=True)

class ListaIngredientesComprados(models.Model):
    idlistaingredientescomprados = models.AutoField(primary_key=True)
    quantidade = models.IntegerField() 
    idfornecedor = models.ForeignKey('Fornecedor', models.DO_NOTHING, db_column='idfornecedor', blank=True, null=True)
    idingrediente = models.ForeignKey('Ingrediente', models.DO_NOTHING, db_column='idingrediente', blank=True, null=True)
    idcompra_ingrediente = models.ForeignKey('CompraIngrediente', models.DO_NOTHING, db_column='idcompra_ingrediente', blank=True, null=True)

